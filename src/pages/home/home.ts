import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AetnaDentalAccessPage } from '../../pages/aetna-dental-access/aetna-dental-access';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  aetnaDentalAccessPage = AetnaDentalAccessPage;

  constructor(public navCtrl: NavController) {

  }

}
