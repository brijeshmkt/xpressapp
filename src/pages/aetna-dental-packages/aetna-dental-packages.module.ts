import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AetnaDentalPackagesPage } from './aetna-dental-packages';

@NgModule({
  declarations: [
    AetnaDentalPackagesPage,
  ],
  imports: [
    IonicPageModule.forChild(AetnaDentalPackagesPage),
  ],
  exports: [
    AetnaDentalPackagesPage
  ]
})
export class AetnaDentalPackagesPageModule {}
