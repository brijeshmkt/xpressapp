import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {AetnaDentalPackagesPage} from '../../pages/aetna-dental-packages/aetna-dental-packages';

@IonicPage()
@Component({
  selector: 'page-aetna-dental-access',
  templateUrl: 'aetna-dental-access.html',
})
export class AetnaDentalAccessPage {
  aetnaDentalPackages = AetnaDentalPackagesPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AetnaDentalAccessPage');
  }

}
