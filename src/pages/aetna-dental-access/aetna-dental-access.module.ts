import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AetnaDentalAccessPage } from './aetna-dental-access';

@NgModule({
  declarations: [
    AetnaDentalAccessPage,
  ],
  imports: [
    IonicPageModule.forChild(AetnaDentalAccessPage),
  ],
  exports: [
    AetnaDentalAccessPage
  ]
})
export class AetnaDentalAccessPageModule {}
